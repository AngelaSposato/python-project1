from archiving_data.db_management import DBManagement

'''
@author Mandeep Singh

This module is in charge of the main program for scraping data
'''
def data_scrape_save_main_program(rootPassword):
    userChoice = input('Enter your choice:\n (1) Save to local web file\n (2) Scrape from a saved local file\nChoice: ')
    if(userChoice =='1'):
        print('Files should already be saved locally in the local_html/ folder')
    elif(userChoice =='2'):
        newChoice = input('Enter your choice:\n (1) Scrape from TWO NEW FILES\n (2) Scrape from default files\n N.B Default files are for days: 03-20 and 03-23\n N.B Make sure your TWO NEW FILES are stored in local_html\nChoice: ')
        if(newChoice =='1'):
            #process to scrape from 2 new files
            print('Process starts for local_page_2021-03-....')
            userDay1 = input('Enter the day in digits(99) for your stored file #1 (e.g. 03) ')
            htmlFile1 = 'local_html/local_page2021-03-{}.html'.format(userDay1)
            print('Process starts for local_page_2021-03-....')
            userDay2 = input('Enter the day in digits(99) for your stored file #2 (e.g. 03) ')
            htmlFile2 = 'local_html/local_page2021-03-{}.html'.format(userDay2)
            db_manage = DBManagement(htmlFile1,htmlFile2,rootPassword)
            db_manage.main()
        elif(newChoice =='2'):
            #calling class that will insert to database with locally saved files
            db_manage = DBManagement('local_html/local_page2021-03-20.html','local_html/local_page2021-03-23.html',rootPassword)
            db_manage.main()
        else:
            print('Enter Valid Choice!')
            userRootPassword = input('Please enter your root password for database\nPassword: ')
            data_scrape_save_main_program(userRootPassword)
    else:
        print('Enter Valid Choice!')
        userRootPassword = input('Please enter your root password for database\nPassword: ')
        data_scrape_save_main_program(userRootPassword)
    

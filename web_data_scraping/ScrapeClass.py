from bs4 import BeautifulSoup
'''
@author Mandeep Singh
This class is in charge of scraping the data from the localfile
'''

class ScrapeClass:
    #ScrapeClass constructor
    #localfileHTML - string representation of the content inside the localhtml file
    #filename - string representation of the localhtml filename
    #three empty lists which will contain scraped data for that specific day
    def __init__(self,localfileHTML,filename):
        self.__localfileHTML = localfileHTML
        self.__filename = filename
        #3 empty lists
        self.today = []
        self.yesterday = []
        self.beforeYesterday = []

    #Method scrapes data from a table (table_id) in the __localfileHTML and stores it into a list (storedLocation).
    #dayRemoveForDate - int value representing how many days to remove to get the date by calling findDate()
    def scrapeData(self,table_id,storedlocation,dayRemoveForDate):
        bs4Obj = BeautifulSoup(self.__localfileHTML,"html.parser")
        countryTable = bs4Obj.find('table', id = '{}'.format(table_id)) 
        #loop through all tbody in table
        for tbody in countryTable.find_all('tbody'):
            #stores all rows from table
            rows = tbody.find_all('tr')
            #Loop through all rows
            for i in range(0,len(rows)):
                #store all data of row number i
                data = rows[i].find_all('td')
                #temporary list 
                tmpList = []
                #loop through all of data
                for j in range(0,len(data)):
                    #store it as text in tmpList
                    tmpList.append(data[j].get_text())
                #if the first element of tmpList is not empty store the date by calling findDate() and the tmpList in storedLocation 
                if tmpList[0]!='':
                    storedlocation.append(self.findDate(dayRemoveForDate)+tmpList)
             
    #Method calls the scrapeData() to inserts scraped data into the corresponding lists for that day
    def createLists(self):
        self.scrapeData("main_table_countries_today",self.today,0)
        self.scrapeData("main_table_countries_yesterday",self.yesterday,-1)
        self.scrapeData("main_table_countries_yesterday2",self.beforeYesterday,-2)
    
    #Methods returns a list with only 1 element representing the date.
    #daysToRemove - int representaion of how many days to remove in order to get the date.
    def findDate(self,daysToRemove):
        #slicing values in order to get the day and month from filename
        month = self.__filename[-10:-7]
        day = (self.__filename[-7:-5])
        listOfDate = []
        #If no days to remove
        if daysToRemove == 0:
            #and day is a single digit
            if(len(day)==1):
                #concatenate 0 before the day
                day = '0'+day
                #append the month+day to listOfDate
                listOfDate.append(month+day)
            #if day not single digit,append the month+day to listOfDate    
            else:
                listOfDate.append(month+str(day))
        #If 1 day to remove
        elif daysToRemove ==-1:
            #remove 1 day
            tmpDay = int(day) - 1
            day = str(tmpDay)
            #if day is a single digit
            if(len(day)==1):
                #concatenate 0 before the day
                day = '0'+day
                #append the month+day to listOfDate
                listOfDate.append(month+day)
            #if day not single digit,append the month+day to listOfDate  
            else:
                listOfDate.append(month+str(day))
        #If 2 days to remove
        elif daysToRemove == -2:
            #remove 2 days
            tmpDay = int(day) - 2
            day = str(tmpDay)
            #if day is a single digit
            if(len(day)==1):
                #concatenate 0 before the day
                day = '0'+day
                #append the month+day to listOfDate
                listOfDate.append(month+day)
            #if day not single digit,append the month+day to listOfDate  
            else:
                listOfDate.append(month+str(day))
        return listOfDate
            
    #returns a list containing the scrape data of today
    def getTodayData(self):
        return self.today

    #returns a list containing the scrape data of yesterday
    def getYesterdayData(self):
        return self.yesterday

    #returns a list containing the scrape data of before yesterday
    def getBeforeYesterdayData(self):
        return self.beforeYesterday
    
    

        
        

    
    
    


        
'''
@author Mandeep Singh
This class is in charge of FileIO object
'''
class FileIO:
    #FileIO contructor
    #filename -string representation of the local filename
    def __init__(self,filename):
        self.__filename = filename
        self.__filestr= ""
        self.__fileobj = open(filename,encoding='utf8')

    #Method inserts each line from the fileobj to the filestr, creating a long string
    def write2filestr(self):
        myLines = self.__fileobj.readlines()
        for line in myLines:
            self.__filestr += line

    #returns a file object of the file
    def getFileObj(self):
        return self.__fileobj

    #returns a long string containing each line from the fileobj
    def getFileString(self):
        return self.__filestr
        
    #returns a string representaion of the local filename    
    def getFilename(self):
        return self.__filename
    
   
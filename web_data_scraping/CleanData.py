from bs4 import BeautifulSoup
'''
@author Mandeep Singh
This class is in charge of cleaning the data from the scraped data
'''

class CleanData:
    #CleanData constructor
    #scrapedData - a list containing the scrapedData from the ScrapeClass
    def __init__(self,scrapedData):
        #empty list of tuples which will contain the cleanedData
        self.__cleanedData = []
        self.__scrapedData = scrapedData
        
    #cleans the scrapedData into proper format and converts it into tuple
    def cleanData(self):
        #for each row in scrapedData
        for row in self.__scrapedData:
            #loop through each element in a row
            for i in range(0,len(row)):
                #remove +
                row[i] = row[i].replace('+','')
                #remove ,
                row[i] = row[i].replace(',','')
                #remove ' '
                row[i] = row[i].replace(' ','')
            #replace empty values with None    
            row = [str(i or None) for i in row]    
            #append the cleaned row to cleanedData    
            self.__cleanedData.append(row)
            #convert the __cleanedData to a list of tuples
            self.convertToTuple()

    #converts the list of clean data into a list of tuple
    def convertToTuple(self):
        #temporary lists
        tmpRecords = []
        tmpData = []
        #for each record in cleanedData
        for record in self.__cleanedData:
            #loop through each element in a record
            for i in range(0,len(record)):
                #if it's a int value
                if(isinstance(record[i],int)):
                    #append it to tmpRecords
                    tmpRecords.append(record[i])
                #else if it's a 'None', None or 'N/A' value 
                elif(record[i]=='None' or record[i]==None or record[i]=='N/A'):
                    #append None to tmpRecords
                    tmpRecords.append(None)    
                #else if the string element contains a digit
                elif(record[i].isdigit()):
                    #append it as int value
                    tmpRecords.append(int(record[i])) 
                #else append it as a string
                else:
                    tmpRecords.append(str(record[i]))
            #transform the newly cleanedData into a tuple   
            tmpTuple = (tmpRecords[0],tmpRecords[1],tmpRecords[2].capitalize(),tmpRecords[3],tmpRecords[4],
            tmpRecords[5],tmpRecords[6],tmpRecords[7],tmpRecords[8],tmpRecords[9],tmpRecords[10],tmpRecords[11],
            tmpRecords[12],tmpRecords[13],tmpRecords[14],tmpRecords[15],tmpRecords[16],tmpRecords[17],tmpRecords[18],tmpRecords[19])
            #append the tmpTuple into tmpData
            tmpData.append(tmpTuple)
            #clear the tmpRecords to make it empty
            tmpRecords.clear()
        #copy the list of tuples into cleanedData
        self.__cleanedData = tmpData[:]

    #returns a list of tuples containing the cleaned data
    def getCleanData(self):
        return self.__cleanedData

    #prints each element in the cleanedData list
    def printCleanData(self):
            for row in self.__cleanedData:
                print(row)
                
           

    

    
        


        

# -*- coding: utf-8 -*-
"""
Created on Sun Mar 28 20:47:32 2021

@author: Angela, Mandeep
"""
import pandas
import matplotlib.pyplot as plt
import numpy as np
import mysql.connector
from archiving_data.db_api import DataBaseAPI


def connect_db(db_name,rootPassword):
     db=DataBaseAPI('localhost', 'root', rootPassword)
     conn= db.db_connection()
     db.select_db(db_name)
     return conn
 
#QUESTION 7 - AUTHOR: Mandeep
def analyse_6days_country(country,rootPassword):
    try:
        #query to retrieve info
        query = 'SELECT theDate,countryOther,newCases,newDeaths,newRecovered FROM corona_table WHERE countryOther ="{}"'.format(country.capitalize())    
        df = pandas.read_sql_query(query,connect_db('covid_corona_db_spos_sing',rootPassword))
        if(len(df)) !=6:
            print('{} does not exist!'.format(country))
        else:
            #plotting and labelling
            df.plot(kind='bar',x='theDate',y=['newCases','newRecovered','newDeaths'],color=['blue','green','red'],width=0.8)
            plt.legend(loc='upper right')
            plt.xticks(rotation=None)
            plt.xlabel('date')
            plt.ylabel('3-main Indicators')
            titleStr = ('6-days key indicators evolution {}').format(country)
            plt.title(titleStr)
            plt.show()
    except mysql.connector.Error as err:
        print('Database has problem: {}'.format(err))
    except Exception as e:
        print('Exception Found in analyse_6days_country(): {}'.format(e))

#QUESTION 8 - AUTHOR: Angela

#returns query to be run that gets a country's neighboring country
def generate_border_query(country):
     return 'select neighbouringCountry from country_borders_table where sharedBorder=(select max(sharedBorder) from country_borders_table where countryName="{}") and countryName= "{}"'.format(country, country)
 
#returns query that gets the date and new cases of the country    
def generate_covid_number_query(country):
    return 'select theDate, newCases from corona_table where countryOther="{}" order by theDate ASC'.format(country)

#generates plot by getting data from DB with help of above queries
def generate_plot_6days(country,rootPassword):
    try:
        neighboring_country=pandas.read_sql_query(generate_border_query(country), connect_db('covid_corona_db_spos_sing',rootPassword))

        if len(neighboring_country) == 0:
            print('{} does not have a neighbor'.format(country))
        else:
            neighbor1=neighboring_country.get('neighbouringCountry')[0]
            
            #using read_sql_query to retrieve data from DB
            main_country_covid=pandas.read_sql_query(generate_covid_number_query(country), connect_db('covid_corona_db_spos_sing',rootPassword))
            neighbor_covid=pandas.read_sql_query(generate_covid_number_query(neighbor1), connect_db('covid_corona_db_spos_sing',rootPassword))
            
            x=np.arange(len(main_country_covid.theDate))
            
            #adding +.2 and -.2 so that each bar is next to each other
            plt.bar(x-0.2, main_country_covid.newCases, width=0.4, label='New cases-{}'.format(country),color='blue')
            plt.bar(x+0.2, neighbor_covid.newCases, width=0.4, label='New cases-{}'.format(neighbor1),color='red')
            
            #labels and legend
            plt.xticks(x,main_country_covid.theDate)
            plt.title('6 Days New Cases Comparison with neighbor {}'.format(neighbor1))
            plt.xlabel('Date')
            plt.ylabel('New Cases')   
            
            plt.legend()
            plt.show()

    except mysql.connector.Error as err:
        print('Database has problem: {}'.format(err))
    except Exception as e:
        print('Exception Found in generate_plot_6days(): {}'.format(e))

        
#QUESTION 9 - AUTHOR: Angela

#returns query that gets 3 neighboring countries
def generate_many_borders_query(country):
    return 'select neighbouringCountry from country_borders_table where countryName="{}"order by sharedBorder desc limit 3'.format(country)
    
#returns query that gets the deaths, the date, and the country with that death rate
def generate_deaths1m_query(country):
    return '''
    select deaths1MPop, theDate, countryOther from corona_table 
    where countryOther="{}"
    order by countryOther asc
    '''.format(country)

        
#generates plot of 6 days deaths 1M/Pop for a country and 3 of its neighbors                         
def generate_plot_deaths1m(country,rootPassword):
    try:
        country_neighbors=pandas.read_sql_query(generate_many_borders_query(country), connect_db('covid_corona_db_spos_sing',rootPassword))
        if len(country_neighbors) != 3:
            print('{} does not have 3 neighbors'.format(country))
        else:
            #getting all the neighbors
            neighbor1=country_neighbors.get('neighbouringCountry')[0]
            neighbor2=country_neighbors.get('neighbouringCountry')[1]
            neighbor3=country_neighbors.get('neighbouringCountry')[2]
            
            #getting deaths for main country and 3 neighbors
            deaths1m_country=pandas.read_sql_query(generate_deaths1m_query(country), connect_db('covid_corona_db_spos_sing',rootPassword))
            deaths1m_n1=pandas.read_sql_query(generate_deaths1m_query(neighbor1), connect_db('covid_corona_db_spos_sing',rootPassword))
            deaths1m_n2=pandas.read_sql_query(generate_deaths1m_query(neighbor2), connect_db('covid_corona_db_spos_sing',rootPassword))
            deaths1m_n3=pandas.read_sql_query(generate_deaths1m_query(neighbor3), connect_db('covid_corona_db_spos_sing',rootPassword))
                
            x=np.arange(len(deaths1m_country.theDate))
            
            #plotting
            plt.bar(x-0.4, deaths1m_country.deaths1MPop, width=0.2, label='Deaths/1M Pop-{}'.format(country),color='blue')
            plt.bar(x-0.2, deaths1m_n1.deaths1MPop, width=0.2, label='Deaths/1M Pop-{}'.format(neighbor1),color='red')
            plt.bar(x, deaths1m_n2.deaths1MPop, width=0.2, label='Deaths/1M Pop-{}'.format(neighbor2),color='green')
            plt.bar(x+0.2, deaths1m_n3.deaths1MPop, width=0.2, label='Deaths/1M Pop-{}'.format(neighbor3),color='cyan')
            
            #labelling and ticks
            plt.xticks(x,deaths1m_country.theDate)
            plt.title('6 Days Deaths 1M/Pop comparison {} with neighbours'.format(country))
            plt.xlabel('Date')
            plt.ylabel('Deaths/1M pop')

            plt.legend()
            plt.show()

    except mysql.connector.Error as err:
        print('Database has problem: {}'.format(err))
    except Exception as e:
        print('Exception Found in generate_plot_deaths1m(): {}'.format(e))

def data_explore_analyse_main_program(rootPassword):
    userCountry = input('Please enter a country to explore: ').capitalize() 
    analyse_6days_country(userCountry,rootPassword)
    generate_plot_6days(userCountry,rootPassword)
    generate_plot_deaths1m(userCountry,rootPassword)


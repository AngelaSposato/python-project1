To run this project:

1. Open and run main_program.py

2. You will be prompted to enter your password for your local MySQL database; enter your password.

3. First, enter S to scrape the data from the HTML files and save to the database. 

4. To scrape information from a saved local file, enter 2. 

5. If you would like to scrape from 2 new files, make sure they are in the local_html folder and enter 1. Enter 2 to scrape from the default files that are already in that folder. This will scrape the files and insert them into a database.

6. Run main_program.py once again.

7. This time, enter X to explore countries.

8. You will be prompted to enter a country of your choice, and 3 graphs will be generated of COVID data on the countries you have entered. Note that some countries may be missing data or have less than 3 neighboring countries.

9. Enjoy!

Made with love and Python by:

Angela Sposato 
Student ID: 1934695
GitLab username: @AngelaSposato

Mandeep Singh 
Student ID: 1937332
GitLab username: @m.singh13
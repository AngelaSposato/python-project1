from data_explore_analyze import data_explore_analyse_main_program
from data_scrape_save import data_scrape_save_main_program

'''
@author: Mandeep Singh

This module is for the main program
'''
def main_program():
    userRootPassword = input('Please enter your root password for database\nPassword: ')
    userChoice = input('What do you want to do?\n (S): Scrape and save to database\n (X): Explore analyse data\nChoice: ')
    if userChoice.upper() =='S':
        data_scrape_save_main_program(userRootPassword)
    elif userChoice.upper() =='X':
        data_explore_analyse_main_program(userRootPassword)
    else:
        print('Please enter a valid choice!')
        main_program()
    
if __name__ == '__main__':
    main_program()
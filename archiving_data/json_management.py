# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 15:23:25 2021

@author: Angela 
"""

import json

class ManageJSON:
    def __init__(self):
        self.data=[]
    
    #reading from file and returning a list of dictionaries
    def load_json(self):
       json_file=open('local_json/country_neighbour_dist_file.json')
       #list of dict
       data=json.load(json_file)
       return data
   
   #taking list of dictionaries and putting in suitable form to be inserted in SQL table
    def sanitize_json(self, data):
        list_json=[]
        #iterating through each index in list
        for i in range(len(data)):
            #iterating through each index of dict in list
            for x in data[i]:
                #getting keys and values from dict
                k=data[i][x].keys()
                v=data[i][x].values()
                #if the data has missing values, (no neighboring countries in this case), replacing with None so it will be null in db
                if len(data[i][x])==0:
                    k=None
                    v=None
                    #creating tuple with these values and appending to list for database entry
                    tpl_json=(x, k, v)
                    list_json.append(tpl_json)
                else:
                    for k, v in data[i][x].items():
                        tpl_json=(x, k, v) 
                        list_json.append(tpl_json)
        return list_json
 

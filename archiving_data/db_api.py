# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 16:36:14 2021

@author: Angela
"""
import mysql.connector

class DataBaseAPI:
    def __init__(self, host, user, password):
        self.host=host
        self.user=user
        self.password=password
        self.conn=None

    def db_connection(self):
        self.conn=mysql.connector.connect(
            host=self.host,
            user=self.user,
            password=self.password)
        return self.conn
    
    #returns a cursor that is used heavily in this method
    def get_cursor(self):
        my_cursor=self.conn.cursor()
        return my_cursor
    
    #creates a database
    def create_db(self, db_name):
        try: 
            my_cursor=self.get_cursor()
            my_cursor.execute('DROP DATABASE IF EXISTS {}'.format(db_name))
            my_cursor.execute('CREATE DATABASE {}'.format(db_name))
            self.conn.commit()
            my_cursor.close()
        except mysql.connector.Error as err:
                print('Database has problem: {}'.format(err))
                
    #selects a database to work with
    def select_db(self, db_name):
        try:
            my_cursor=self.get_cursor()
            my_cursor.execute('USE {}'.format(db_name))
            my_cursor.close()
        except mysql.connector.Error as err:
            print('Database has problem: {}'.format(err))
        
    #creates a table
    def create_table(self, table_name, table_schema):
        try:
            query='CREATE TABLE {} ({})'.format(table_name, table_schema)
            my_cursor=self.get_cursor()
            my_cursor.execute('DROP TABLE IF EXISTS {}'.format(table_name))
            my_cursor.execute(query)
            self.conn.commit()
            my_cursor.close()
        except mysql.connector.Error as err:
            print('Database has problem: {}'.format(err))
    
    #inserts data into table
    def populate_table(self, table_name, table_schema, list_tuples):
        try: 
            #string format taking into consideration the length of the tuple
            string_format='%s, '*(len(list_tuples[0])-1)
            #the last entry of tuple cannot end with comma
            last_string_format='%s'
            #query 
            insert_query='INSERT INTO {} ({}) VALUES ({} {})'.format(table_name, table_schema, string_format, last_string_format)
            my_cursor=self.get_cursor()
            my_cursor.executemany(insert_query, list_tuples)
            self.conn.commit()
            my_cursor.close()
        except mysql.connector.Error as err:
            print('Database has problem: {}'.format(err))
        
    def retrieve_data(self, query):
        try:
            my_cursor=self.get_cursor()
            my_cursor.execute(query)
            data=my_cursor.fetchall()
            my_cursor.close()
            return data
        except mysql.connector.Error as err:
            print('Database has problem: {}'.format(err))
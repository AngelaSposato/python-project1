# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 17:35:18 2021

@author: Angela
"""
from archiving_data.db_api import DataBaseAPI
from archiving_data.json_management import ManageJSON
from web_data_scraping.FileIO import FileIO
from web_data_scraping.ScrapeClass import ScrapeClass
from web_data_scraping.CleanData import CleanData


class DBManagement:
    def __init__(self,file1,file2,rootPassword):
        self.data=[]
        self.file1=file1
        self.file2=file2
        self.rootPassword=rootPassword
    
    def establish_connection(self):
        db=DataBaseAPI('localhost', 'root', self.rootPassword)
        db.db_connection()
        return db
    
    #creates database
    def create_db(self, db):
        db.create_db('covid_corona_db_SPOS_SING')
        db.select_db('covid_corona_db_SPOS_SING')
        
    #creates the 2 tables 
    def create_tables(self, db):
        db.create_table('corona_table', 
                        '''
                        theDate varchar(15) NOT NULL,
                        ranking int(11) NOT NULL,
                        countryOther varchar(50) NOT NULL,
                        totalCases int(20),
                        newCases int(20),
                        totalDeaths int(20),
                        newDeaths int(20),
                        totalRecovered int(20), 
                        newRecovered int(20),
                        activeCases int(20), 
                        seriousCritical int(20),
                        totCases1MPop int(20),
                        deaths1MPop int(20),
                        totalTests int(20),
                        tests1MPop int(20),
                        population int(20),
                        continent varchar(50),
                        1CaseEveryXPpl int(20),
                        1DeathEveryXPpl int(20),
                        1TestEveryXPpl int(20),
                        PRIMARY KEY(theDate,countryOther)
                        ''')
        db.create_table('country_borders_table', 
                                 '''
                                 countryName varchar(50),
                                 neighbouringCountry varchar(50), 
                                 sharedBorder int(5)
                                 ''')
    
    #scraping JSON and inserting into table                             
    def insert_json_data(self, db):             
        json_manager=ManageJSON()
        data=json_manager.load_json()
        json_tpl= json_manager.sanitize_json(data)
        db.populate_table('country_borders_table', 'countryName, neighbouringCountry, sharedBorder', json_tpl)
    
    #scrapes file and returns content
    def scrape_local_file(self,filename):
        local_html_file = FileIO(filename)
        local_html_file.write2filestr()
        scrape_html_file = ScrapeClass(local_html_file.getFileString(),local_html_file.getFilename())
        scrape_html_file.createLists()
        return scrape_html_file
  
    def get_all_data_from_file(self,filename):
        #Scrape the file
        local_html = self.scrape_local_file(filename)
        #Create CleanData object for the 3 days in the file
        before_yesterday_data = CleanData(local_html.getBeforeYesterdayData())
        yesterday_data = CleanData(local_html.getYesterdayData())
        today_data = CleanData(local_html.getTodayData())
        #Clean the data for the 3 days in the file
        before_yesterday_data.cleanData()
        yesterday_data.cleanData()
        today_data.cleanData()
        #Add all 3 cleaned days into one list of tuples
        all_data = before_yesterday_data.getCleanData() + yesterday_data.getCleanData() + today_data.getCleanData()
        return all_data
    
    #this method calls other methods to scrape from 2 local files and merges them together to be inserted into database table
    def get_covid_data(self):
        data_from_localfile1 = self.get_all_data_from_file(self.file1)
        data_from_localfile2 = self.get_all_data_from_file(self.file2)
        data_from_localfiles = data_from_localfile1 + data_from_localfile2
        return data_from_localfiles
    
    #inserts scraped COVID data into table
    def insert_covid_data(self, db):
        all_data = self.get_covid_data()
        db.populate_table('corona_table', 
                          '''
                          theDate, 
                          ranking, 
                          countryOther, 
                          totalCases, 
                          newCases,
                          totalDeaths, 
                          newDeaths, 
                          totalRecovered, 
                          newRecovered, 
                          activeCases, 
                          seriousCritical, 
                          totCases1MPop, 
                          deaths1MPop,
                          totalTests,
                          tests1MPop,
                          population, 
                          continent, 
                          1CaseEveryXPpl,
                          1DeathEveryXPpl,
                          1TestEveryXPpl''',
                          all_data)
        
    #calls all the methods to create database, create tables, and insert data
    def main(self):
        db=self.establish_connection()
        self.create_db(db)
        self.create_tables(db)
        self.insert_json_data(db)
        print('inserting to database...')
        self.insert_covid_data(db)
        print('Files stored!')

